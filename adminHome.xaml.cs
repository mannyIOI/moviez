﻿using Microsoft.Win32;
using MovieZ;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using WpfApp2.Models;

namespace WpfApp2 {
	public partial class adminHome : Page {
		ProgressBar loadPB;
		public adminHome() {
			InitializeComponent();
			//loadPB = loading;
			//loadPB.Visibility = Visibility.Hidden;
			//behaviors
		}

		private void addFeatureMovie(object sender, RoutedEventArgs e) {
			MyFeature ft = null;
			try {
                //UTILITY.db.addFeature(new MyFeature(feature_title.Text));
                ft = new MyFeature(feature_title.Text);
                MessageBox.Show("Enter file location for " + ft.title);
				ft.setFileLocation(getFileLocation());
				UTILITY.db.addFeature(ft);
				AdminContentPage.Navigate(new MovieAdded(ft));
			}
			catch (MySqlException ex) {
				if (ex.Number == 1062) {
					MessageBox.Show(ft.title + " is already in the trending list");
				}
				else {
					MessageBox.Show("" + ex.Number);
				}
			}
			catch (HttpRequestException ex) {
				//if(ex.Message)
				MessageBox.Show(ex.Message + " ... Please check your spelling");
				//if()
			}
			catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}

		private void AddTrending(object sender, RoutedEventArgs e) {
			MyFeature ft = null;
			try {
                ft = new MyFeature(trending_title.Text);
                MessageBox.Show("Enter file location for " + ft.title);
				UTILITY.db.addTrending(ft.setFileLocation(getFileLocation()));
				AdminContentPage.Navigate(new MovieAdded(ft));
				//MessageBox.Show("Success");
			}
			catch (MySqlException ex) {
				if (ex.Number == 1062) {
					MessageBox.Show("That movie is already in the trending list");
				}
				else {
					MessageBox.Show(""+ex.Number);
				}
			}
			catch (Exception ex) {
				MessageBox.Show("Sorry could not figure that one out");
			}
		}

		private void addEpisode(object sender, RoutedEventArgs e) {
			try {
				//this.loadPB.Visibility = Visibility.Visible;
				addEpisode();
				//this.loadPB.Visibility = Visibility.Hidden;
			}
			catch (MySqlException ex) {
				MessageBox.Show(ex.ToString());
			}
			catch (HttpRequestException ex) {
				MessageBox.Show(ex.Message);
			}
			catch (Exception ex) {
				MessageBox.Show(ex.ToString());
			}
		}

		private void Logout(object sender, RoutedEventArgs e) {
			NavigationService.Navigate(new Login());
		}

		public void addEpisode() {
			try {
				MyEpisode episode = new MyEpisode(series_title.Text,
						Convert.ToInt32(season_number.Text), Convert.ToInt32(episode_number.Text));

				episode.setFileLocation(getFileLocation());
				UTILITY.db.addEpisode(episode);
				series_title.Text = "";
				season_number.Text = "";
				episode_number.Text = "";
				AdminContentPage.Navigate(new MovieZ.SeriesAdded(episode));
			}
			catch(Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}

		private void ViewCheckOut(object sender, RoutedEventArgs e) {
			//MessageBox.Show("Hello");
			try {
				List<List<CheckedOut>> fromDb = UTILITY.db.getCheckOut(Convert.ToInt32(user_id.Text));
				List<CheckedOut> final = new List<CheckedOut>();
				for (int i = 0; i < fromDb[0].Count; i++) {
					final.Add(fromDb[0][i]);
				}
				for (int i = 0; i < fromDb[1].Count; i++) {
					final.Add(fromDb[1][i]);
				}
				AdminContentPage.Navigate(new CheckedOutItemsList(final, user_id.Text));

			}
			catch(FormatException ex) {
				MessageBox.Show("Please enter only Id numbers");
			}
		}
		public string getFileLocation() {
			OpenFileDialog fileDialog = new OpenFileDialog();
			//choofdlog.Filter = "MP4 (*.mp4*)|*.*";
			fileDialog.FilterIndex = 1;
			fileDialog.Multiselect = false;
			//fileDialog.
			if (fileDialog.ShowDialog().Value) {
				return fileDialog.FileName;
			}
			return null;
		}

		private void series_title_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == System.Windows.Input.Key.Enter) {
				season_number.Focusable = true;
				season_number.Focus();
			}
		}

		private void season_number_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == System.Windows.Input.Key.Enter) {
				episode_number.Focusable = true;
				episode_number.Focus();
			}
		}

		private void episode_number_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == System.Windows.Input.Key.Enter) {
				addEpisode();
			}
		}
	}
}
