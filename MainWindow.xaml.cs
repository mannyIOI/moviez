﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Input;

namespace WpfApp2 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
			//Ma
			//MainPage.Content = new Login();
			MainPage.Navigate(new Login());
		}

		private void Button_Click(object sender, RoutedEventArgs e) {
			closeMessage.IsOpen = true;
		}

		private void CloseClicked(object sender, RoutedEventArgs e) {
			this.Close();
		}


		protected override void OnActivated(EventArgs e) {
			base.OnActivated(e);
			if (WindowStyle != WindowStyle.None) {
				Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, (DispatcherOperationCallback) delegate (object unused)
				{
					WindowStyle = WindowStyle.None;
					return null;
				}
				, null);
			}
		}

		private void minimizeButton_Click(object sender, RoutedEventArgs e) {
			WindowStyle = WindowStyle.SingleBorderWindow;
			WindowState = WindowState.Minimized;
		}

		private void closeMessage_KeyDown(object sender, KeyEventArgs e) {
			if (e.Key != Key.Enter)
				return;

			MessageBox.Show("Enter pressed");
		}
	}
}
