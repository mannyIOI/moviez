﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp2;
using WpfApp2.Models;

namespace MovieZ {
	/// <summary>
	/// Interaction logic for MovieAdded.xaml
	/// </summary>
	public partial class MovieAdded : Page {
		public MovieAdded(MyFeature movie) {
			InitializeComponent();
			movie_added.Children.Add(
				new MovieDetailsUC(movie, true)
				);
		}
	}
}
