﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace WpfApp2 {
	/// <summary>
	/// Interaction logic for Login.xaml
	/// </summary>
	public partial class Login : Page
	{
		public Login()
		{
			InitializeComponent();
		}

		private void Guest_Login(object sender, RoutedEventArgs e) {
			//if (UTILITY.db.noError) {
			try {
				NavigationService.Navigate(new GuestPage());
			}
			catch (Exception ex) {
				System.Windows.MessageBox.Show("Error connecting to the database please start up the server");
			}
		}
		private void Admin_Login(object sender, RoutedEventArgs e) {
			string username = uname.Text;
			string userpass = upass.Password;

			if (Models.UTILITY.db.login(username, userpass))
			{
				NavigationService.Navigate(new adminHome());
			}
			else {
				MessageBox.Show("Wrong username or password");
			}


			//prgBaseMap.Maximum = 10000.0;

			//try {
			//	copyMultiple(
			//		new string[] {
			//			"E:\\Videos\\Feature Movies\\source\\1.mp4",
			//			"E:\\Videos\\Feature Movies\\source\\2.mp4"
			//			}, 
			//		new string[] {
			//			"E:\\Videos\\Feature Movies\\dest\\1.mp4",
			//			"E:\\Videos\\Feature Movies\\dest\\2.mp4"});
			//}
			//catch (Exception ex) {
			//	MessageBox.Show(ex.ToString());
			//}

		}
		
	}
}
