﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WpfApp2.Models;

namespace WpfApp2 {
	public partial class Movies : Page {

		static int offset = 0;
		static List<MyFeature> currentPage;
		int allEpSize = UTILITY.db.getAllFeatureCount();

		public Movies() {
			InitializeComponent();
			fillGrid();
		}

		private void fillGrid() {
			int counter = 0;
			currentPage = UTILITY.db.getAllFeatures(offset);
			clearGrid();
			for (int row = 0; row < 2; row++) {
				for (int col = 0; col < 4; col++) {
					if (( row != 1 || col != 3 ) && counter < currentPage.Count) {
						addItem(currentPage[counter], row, col);
						counter++;
					}
				}
			}
		}

		public void clearGrid() {
			contentGrid.Children.RemoveRange(1, 7);
		}

		public void addItem(MyFeature feature, int row, int col) {
			MovieDetailsUC ftUC = new MovieDetailsUC(feature, false);
			contentGrid.Children.Add(ftUC);
			Grid.SetRow(ftUC, row);
			Grid.SetColumn(ftUC, col);
		}

		private void nextPageMovies(object sender, RoutedEventArgs e) {
			if (offset < allEpSize - 7) {
				offset = offset + 7;
				fillGrid();
			}
			else {
				fillGrid();
				MessageBox.Show("You are at the end of the line");
			}
		}

		private void prevPageMovies(object sender, RoutedEventArgs e) {
			if (offset > 0) {
				offset = offset - 7;
				fillGrid();
			}
			else if (offset > allEpSize) {
				offset = offset - 7;
				fillGrid();
			}
			else {
				offset = 0;
				fillGrid();
			}
		}

		private void goToPageone(object sender, RoutedEventArgs e) {
			offset = 0;
			fillGrid();
		}

		private void ViewCart(object sender, RoutedEventArgs e) {
			string cartString = UTILITY.getCartString();
			cartDetailMessage.Text = cartString;
			ftcart.IsOpen = true;
		}

		private void CheckOutClicked(object sender, RoutedEventArgs e) {
			if (UTILITY.episodeCart.Count + UTILITY.featureCart.Count > 0) {
				int ID = UTILITY.db.addCheckOut();
				user_id_number.Text = "" + ID;
				user_id_display.IsOpen = true;
			}
			else {
				MessageBox.Show("You have not selected any Movies");
			}
		}
	}
}
