﻿namespace WpfApp2.Models {
// checkout class
	public class CheckedOut {
		public string IMDB_id;
		public string title;
		public string file_location;

		public CheckedOut(string IMDB_id, string title, string file_location) {
			this.IMDB_id = IMDB_id;
			this.title = title;
			this.file_location = file_location;
		}
	}
}

