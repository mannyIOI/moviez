﻿using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfApp2.Models {
	public class Database {
		String constructor = "Server=localhost;Uid=root;Pwd=;database=project_movies";
		MySqlConnection conn;
		public bool noError = true;
		public Exception ex;
		public Database() {
			try {
				conn = new MySqlConnection(constructor);
				conn.Open();
			}
			catch (Exception ex) {
				noError = false;
				this.ex = ex;
			}
		}

		public bool login(string username, string userpass) {
			var cmd = conn.CreateCommand();
			cmd.CommandText = "SELECT COUNT(*) AS count FROM `admin` WHERE `admin`.`username` = @username AND `admin`.`password` = @password LIMIT 1";
			cmd.Parameters.AddWithValue("@username", username);
			cmd.Parameters.AddWithValue("@password", userpass);
			using (var reader = cmd.ExecuteReader()) {
				reader.Read();
				int val = reader.GetInt32("count");
				if (val == 1) {
					return true;
				}
				else {
					return false;
				}
			}
		}

		public int getAllFeatureCount() {
			string query = "SELECT COUNT(*) AS count FROM `feature` WHERE 1";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			using (var reader = cmd.ExecuteReader()) {
				reader.Read();
				int val = reader.GetInt32("count");
				return val;
			}
		}

		public int getTrendingCount() {
			string query = "SELECT COUNT(*) AS count FROM `trending` WHERE 1";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			using (var reader = cmd.ExecuteReader()) {
				reader.Read();
				int val = reader.GetInt32("count");
				return val;
			}
		}

		public string addEpisode(MyEpisode ep) {

			this.addSeries(ep.series);

			string query1 = "REPLACE INTO `movies` (`IMDB_id`, `isSeries`, `file_location`) VALUES (@episodeId, '1', @file_location);";
			var cmd1 = conn.CreateCommand();
			cmd1.CommandText = query1;
			cmd1.Parameters.AddWithValue("@episodeId", ep.IMDB_id);
			cmd1.Parameters.AddWithValue("@file_location", ep.file_location);
			cmd1.ExecuteNonQuery();

			string query = "REPLACE INTO `episodes` " +
				"(`IMDB_id`, `series_id`, `season_number`, `episode_number`, `episode_title`, `rating`, `actors`, `released`, `genre`, `plot`, `image`) " +
				"VALUES (@episodeId, @seriesId, @seasonNumber, @episodeNumber, @episodeTitle, " +
					"@episodeRating, @episodeActors, @episodeDate, @episodeGenre, @episodePlot, @image);";

			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@episodeId", ep.IMDB_id);
			cmd.Parameters.AddWithValue("@seriesId", ep.series_id);
			cmd.Parameters.AddWithValue("@seasonNumber", ep.season_number);
			cmd.Parameters.AddWithValue("@episodeNumber", ep.episode_number);
			cmd.Parameters.AddWithValue("@episodeTitle", ep.episode_title);
			cmd.Parameters.AddWithValue("@episodeRating", ep.rating);
			cmd.Parameters.AddWithValue("@episodeActors", ep.actors);
			cmd.Parameters.AddWithValue("@episodeDate", ep.released);
			cmd.Parameters.AddWithValue("@episodeGenre", ep.genre);
			cmd.Parameters.AddWithValue("@episodePlot", ep.plot);
			cmd.Parameters.AddWithValue("@image", ep.image);

			cmd.ExecuteNonQuery();
			return "true";

			//return false;
		}

		public void addSeries(MySeries series) {
			string query = "INSERT IGNORE INTO `series` (`series_id`, `title`, `total_seasons`) " +
									"VALUES (@seriesID, @seriesTitle, @totalSeasons);";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@seriesTitle", series.title);
			cmd.Parameters.AddWithValue("@seriesID", series.series_id);
			cmd.Parameters.AddWithValue("@totalSeasons", series.totalSeasons);

			cmd.ExecuteNonQuery();

		}

		public void addTrending(MyFeature feature) {
			string query1 = "REPLACE INTO `movies` (`IMDB_id`, `isSeries`, `file_location`) VALUES (@episodeId, '1', @file_location);";
			var cmd1 = conn.CreateCommand();
			cmd1.CommandText = query1;
			cmd1.Parameters.AddWithValue("@episodeId", feature.IMDB_id);
			cmd1.Parameters.AddWithValue("@file_location", feature.file_location);
			cmd1.ExecuteNonQuery();

			addFeature(feature);

			string query = "INSERT INTO `trending_list` (`feature_id`) VALUES (@IMDB_id)";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@IMDB_id", feature.IMDB_id);
			cmd.ExecuteNonQuery();
		}

		public void addFeature(MyFeature feature) {
			string query1 = "INSERT IGNORE INTO `movies` (`IMDB_id`, `isSeries`, `file_location`) VALUES (@episodeId, '0', @file_location);";
			var cmd1 = conn.CreateCommand();
			cmd1.CommandText = query1;
			cmd1.Parameters.AddWithValue("@episodeId", feature.IMDB_id);
			cmd1.Parameters.AddWithValue("@file_location", feature.file_location);
			cmd1.ExecuteNonQuery();

			string query = "REPLACE INTO `feature` " +
				"(`IMDB_id`, `title`, `rating`, `released`, `rated`, `genre`, `director`, `actors`, `revenue`, `production`, `plot`, `image`) " +
				"VALUES (@IMDB_id, @title, @rating, " +
				"@released, @rated, @genre, @actors, @actors, @revenue, @production, @plot, @image);";

			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@IMDB_id", feature.IMDB_id);
			cmd.Parameters.AddWithValue("@title", feature.title);
			cmd.Parameters.AddWithValue("@rating", feature.rating);
			cmd.Parameters.AddWithValue("@released", feature.released);
			cmd.Parameters.AddWithValue("@rated", feature.rated);
			cmd.Parameters.AddWithValue("@genre", feature.genre);
			cmd.Parameters.AddWithValue("@actors", feature.actors);
			cmd.Parameters.AddWithValue("@director", feature.actors);
			cmd.Parameters.AddWithValue("@revenue", feature.revenue);
			cmd.Parameters.AddWithValue("@production", feature.productions);
			cmd.Parameters.AddWithValue("@plot", feature.plot);
			cmd.Parameters.AddWithValue("@image", feature.image);

			cmd.ExecuteNonQuery();

		}

		public int addTest(byte[] byteArray) {
			string query = "INSERT INTO `test` (`image`) VALUES (@Image)";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@Image", byteArray);
			return cmd.ExecuteNonQuery();
		}

		public int addCheckOut() {
			string lastIdQuery = "SELECT user_id FROM `checked_out` ORDER BY user_id DESC LIMIT 1 ";
			var cmd = conn.CreateCommand();
			cmd.CommandText = lastIdQuery;
			int lastId = 0;
			using (var reader = cmd.ExecuteReader()) {
				if (!reader.Read()) {
					return -1;
				}
				lastId = reader.GetInt32("user_id") + 1;
			}
			
			for (int i = 0; i < UTILITY.featureCart.Count;i++) {
				string query = $"INSERT INTO `checked_out` ( `user_id`, `movie_id`, `isSeries`, `pending`) " +
					$"VALUES ('{lastId}', '{UTILITY.featureCart[i].IMDB_id}', '0', '1')";
				var cmd2 = conn.CreateCommand();
				cmd2.CommandText = query;
				cmd2.ExecuteNonQuery();
			}
			for (int i = 0; i < UTILITY.episodeCart.Count; i++) {
				string query = $"INSERT INTO `checked_out` ( `user_id`, `movie_id`, `isSeries`, `pending`) " +
					$"VALUES ('{lastId}', '{UTILITY.episodeCart[i].IMDB_id}', '1', '1')";
				var cmd2 = conn.CreateCommand();
				cmd2.CommandText = query;
				cmd2.ExecuteNonQuery();
			}
			return lastId;
		}

		public void addPending(MyFeature feature, int user_id) {
			string query = $"INSERT INTO `feature_checkout` " +
							"(`user_id`, `feature_id`, `pending`)" +
							"VALUES (@user_id, @feature_id, '1');";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@user_id", user_id);
			cmd.Parameters.AddWithValue("@feature_id", feature.IMDB_id);
			cmd.ExecuteNonQuery();
		}

		public byte[] getImageFromTest() {
			string query = "SELECT * FROM `test`";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			byte[] retval = new byte[] { };
			using (MySqlDataReader reader = cmd.ExecuteReader()) {
				if (!reader.Read()) {
					return null;
				}
				byte[] rawData;
				rawData = new byte[2048 * 100];
				//reader.GetString()
				reader.GetBytes(reader.GetOrdinal("image"), 0, rawData, 0, 2048 * 100);
				return rawData;
			}
		}

		public void getTrending() {
			string query = "INSERT INTO `trending`";
		}

		public MyFeature getFeature(string title) {
			string query = "SELECT * FROM `feature` WHERE title LIKE '%"+title+"%' LIMIT 1";
			lock (this) {
				var cmd = conn.CreateCommand();
				cmd.CommandText = query;
				//cmd.Parameters.AddWithValue("@title", title);
				using (var r = cmd.ExecuteReader()) {
					r.Read();
					if (r.HasRows) {
						byte[] rawData;
						rawData = new byte[2048 * 100];
						MySqlDateTime date = r.GetMySqlDateTime("released");
						string date1 = date.Year+"-"+date.Month+"-"+date.Day;
						r.GetBytes(r.GetOrdinal("image"), 0, rawData, 0, 2048 * 100);
						return new MyFeature(r.GetString("IMDB_id"), r.GetString("title"), r.GetString("rating"), date1, r.GetString("rated"),
												r.GetString("genre"), r.GetString("director"), r.GetString("actors"), r.GetString("revenue"), r.GetString("production"),
												r.GetString("plot"), rawData);
					}
					else {
						return null;
					}
				}
			}
		}

		public List<MyFeature> getTrendingList(int offset) {
			string query = "SELECT * FROM `trending_list` AS `trending`, `feature` " +
							"WHERE `trending`.`feature_id` = `feature`.`IMDB_id` ORDER BY `feature`.`released` DESC LIMIT 7 OFFSET "+offset;
			
			List<MyFeature> trendingList = new List<MyFeature>();
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			using (var r = cmd.ExecuteReader()) {
				while (r.Read()) {
					byte[] rawData;
					rawData = new byte[2048 * 100];
					MySqlDateTime date = r.GetMySqlDateTime("released");
					string date1 = date.Year + "-" + date.Month + "-" + date.Day;
					r.GetBytes(r.GetOrdinal("image"), 0, rawData, 0, 2048 * 100);
					trendingList.Add(new MyFeature(r.GetString("IMDB_id"), r.GetString("title"), 
													r.GetString("rating"), date1, r.GetString("rated"),
													r.GetString("genre"), r.GetString("director"), 
													r.GetString("actors"), r.GetString("revenue"), 
													r.GetString("production"), r.GetString("plot"), 
													rawData));
				}
			}
			return trendingList;
			
		}

		public MyEpisode getEpsiode(string title, int seasonNumber, int episodeNumber) {
			string query = "SELECT * FROM `series`, `episodes` WHERE series.series_id = episodes.series_id " +
								"AND `series`.`title` LIKE '" + title + "' " +
								"AND episodes.episode_number = " + episodeNumber + " " +
								"AND episodes.season_number = " + seasonNumber + "";

			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			using (var r = cmd.ExecuteReader()) {
				r.Read();
				if (r.HasRows) {
					byte[] rawData;
					//FileSize = reader.GetUInt32(reader.GetOrdinal("image"));
					rawData = new byte[2048 * 100];

					r.GetBytes(r.GetOrdinal("image"), 0, rawData, 0, 2048 * 100);

					return new MyEpisode(r.GetString("IMDB_id"), r.GetString("series_id"), 
						r.GetString("title"), r.GetInt32("season_number"),
						r.GetInt32("episode_number"), r.GetString("episode_title"), 
						r.GetString("rating"), r.GetString("actors"),
						r.GetString("released"), r.GetString("genre"),
						r.GetString("plot"), r.GetInt32("total_seasons"),
						rawData);
				}
				else {
					return null;
				}
			}
		}

		public MySeries getSeries(string title) {
			string query = "SELECT * FROM `series` WHERE `title` LIKE @title";

			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@title", title);
			using (var r = cmd.ExecuteReader()) {
				r.Read();
				return new MySeries(r.GetString("series_id"), r.GetString("title"), r.GetInt32("total_seasons"));
			}
		}

		public int getAllEpisodesCount() {
			string query = "SELECT COUNT(*) AS `count`FROM `episodes` WHERE 1";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			using (var r = cmd.ExecuteReader()) {
				r.Read();
				return r.GetInt32("count");
			}
		}

		public List<MyFeature> getAllFeatures(int index) {
			string query = "SELECT * FROM `feature` ORDER BY `feature`.`released` DESC LIMIT 7 OFFSET @index";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@index", index);
			List<MyFeature> featureList = new List<MyFeature>();

			using (var r = cmd.ExecuteReader()) {
				while (r.Read()) {
					byte[] rawData = new byte[2048 * 100];

					MySqlDateTime date = r.GetMySqlDateTime("released");
					string date1 = date.Year + "-" + date.Month + "-" + date.Day;
					r.GetBytes(r.GetOrdinal("image"), 0, rawData, 0, 2048 * 100);
					featureList.Add(new MyFeature(r.GetString("IMDB_id"),
													r.GetString("title"), r.GetString("rating"), date1, 
													r.GetString("rated"), r.GetString("genre"), r.GetString("director"), 
													r.GetString("actors"), r.GetString("revenue"), r.GetString("production"),
													r.GetString("plot"), rawData));
				}
			}
			return featureList;

		}

		public List<MyEpisode> getAllEpisodes(int index) {
			string query = "SELECT * FROM `series`, `episodes` " +
				"WHERE series.series_id = episodes.series_id " +
				"ORDER BY `episodes`.`released` " +
				"DESC LIMIT 7 OFFSET @index";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@index", index);
			List<MyEpisode> episodeList = new List<MyEpisode>();
			using (var r = cmd.ExecuteReader()) {
				while (r.Read()) {
					byte[] rawData;
					rawData = new byte[2048 * 100];
					r.GetBytes(r.GetOrdinal("image"), 0, rawData, 0, 2048 * 100);
					episodeList.Add(new MyEpisode(r.GetString("IMDB_id"), r.GetString("series_id"), r.GetString("title"), r.GetInt32("season_number"),
						r.GetInt32("episode_number"), r.GetString("episode_title"), r.GetString("rating"), r.GetString("actors"),
						r.GetString("released"),
						r.GetString("genre"),
						r.GetString("plot"),
						r.GetInt32("total_seasons"),
						rawData));
				}
			}
			return episodeList;
		}

		public List<List<CheckedOut>> getCheckOut(int user_id) {
			string getSeriesQuery = "SELECT episodes.IMDB_id, episodes.episode_number, episodes.season_number, series.title, movies.file_location " +
				"FROM episodes, checked_out, series, movies " +
				"WHERE checked_out.isSeries = 1 " +
				"AND episodes.series_id = series.series_id " +
				"AND movies.IMDB_id = episodes.IMDB_id " +
				"AND checked_out.movie_id = episodes.IMDB_id " +
				"AND checked_out.user_id = @user_id";

			string getFeatureQuery = "SELECT feature.IMDB_id, feature.title, movies.file_location FROM feature, checked_out, movies " +
				"WHERE checked_out.user_id = @user_id " +
				"AND feature.IMDB_id = checked_out.movie_id " +
				"AND feature.IMDB_id = movies.IMDB_id " +
				"AND checked_out.isSeries = 0 ";

			List<List<CheckedOut>> returnVal = new List<List<CheckedOut>>();

			List<CheckedOut> featureChList = new List<CheckedOut>();
			var featureCmd = conn.CreateCommand();
			featureCmd.CommandText = getFeatureQuery;
			featureCmd.Parameters.AddWithValue("user_id", user_id);
			using (var r = featureCmd.ExecuteReader()) {
				while (r.Read()) {
					featureChList.Add(new CheckedOut(r.GetString("IMDB_id"), r.GetString("title"), r.GetString("file_location")));
				}
			}

			List<CheckedOut> seriesChList = new List<CheckedOut>();
			var seriesCmd = conn.CreateCommand();
			seriesCmd.CommandText = getSeriesQuery;
			seriesCmd.Parameters.AddWithValue("user_id", user_id);
			using (var r = seriesCmd.ExecuteReader()) {
				while (r.Read()) {
					seriesChList.Add(
						new CheckedOut(
							r.GetString("IMDB_id"),
							r.GetString("title") + " Season " + r.GetString("season_number") + " EP " + r.GetString("episode_number"),
							r.GetString("file_location")));
				}
			}
		   
			returnVal.Add(featureChList);
			returnVal.Add(seriesChList);

			return returnVal;
		}

		public bool removeCheckout(string user_id) {
			string query = "UPDATE `checked_out` " +
				"SET `pending` = '0' WHERE " +
				"`checked_out`.`user_id` = @user_id ";
			var cmd = conn.CreateCommand();
			cmd.CommandText = query;
			cmd.Parameters.AddWithValue("@user_id", user_id);
			cmd.ExecuteNonQuery();
			return true;
		}

	}

	
}
