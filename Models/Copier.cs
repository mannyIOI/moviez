﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace WpfApp2.Models {
	public static class Copier {
		public static async Task CopyFiles( List<List<string>> files, Action<double> progressCallback) {
			//Dictionary<string, string> files,
			List<List<string>> list = new List<List<string>>();
			//long total_size = files.Select(y => new FileInfo(y[0]).Length).Sum();
			//long total_size = files.Keys.Select(x => new FileInfo(x).Length).Sum();
			long total_size = 0;
			foreach (var item in files) {
				total_size += new FileInfo(item[0]).Length;
			}
			//return total_size;

			long total_read = 0;

			double progress_size = 10000.0;

			foreach (var item in files) {
				long total_read_for_file = 0;
				var from = item[0];
				var to = item[1];

				// Create the file.
				FileStream fs = File.Create(to);
				fs.Close();

				using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read)) {
					using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read)) {
						await CopyStream(inStream, outStream, x => {
							total_read_for_file = x;
							progressCallback(( ( total_read + total_read_for_file ) / (double) total_size ) * progress_size);
						});
					}
				}

				total_read += total_read_for_file;
			}
		}

		public static async Task CopyStream(Stream from, Stream to, Action<long> progress) {
			int buffer_size = 10240;

			byte[] buffer = new byte[buffer_size];

			long total_read = 0;

			while (total_read < from.Length) {
				int read = await from.ReadAsync(buffer, 0, buffer_size);

				await to.WriteAsync(buffer, 0, read);

				total_read += read;

				progress(total_read);
			}
		}
	}
}
