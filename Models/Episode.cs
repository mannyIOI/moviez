﻿using OMDbApiNet.Model;
using System;
using static WpfApp2.Models.UTILITY;

namespace WpfApp2.Models {
	public class MyEpisode : IMovie {
		public string IMDB_id;
		public string series_id;
		public string series_title;
		public MySeries series;
		public int season_number;
		public int episode_number;
		public string episode_title;
		public string rating;
		public string actors;
		public string released;
		public string genre;
		public string plot;
		public string total_seasons;
		public byte[] image;
		public string file_location;
		private Episode episode;
		public bool chk = false;


		public MyEpisode(string title, int season_number, int episode_number) {
			MyEpisode ep = UTILITY.db.getEpsiode(title, season_number, episode_number);
			if (ep == null) {
				episode = omdb.GetEpisodeBySeriesTitle(title, season_number, episode_number);
				initiate();
			}
			else {
				initiateFromDB(ep);
			}
		}
		public MyEpisode getFromDb(string title, int season_number, int episode_number) {
			return UTILITY.db.getEpsiode(title, season_number, episode_number);
		}
		public MyEpisode(string IMDB_id, string series_id, string series_title, int season_number,
			int episode_number, string episode_title, string rating, string actors, string released, string genre, string plot,
			int total_seasons, byte[] image) {

			this.IMDB_id = IMDB_id;
			this.series_id = series_id;
			this.series_title = series_title;
			this.season_number = season_number;
			this.episode_number = episode_number;
			this.episode_title = episode_title;
			this.rating = rating;
			this.actors = actors;
			this.released = released;
			this.genre = genre;
			this.plot = plot;
			this.series = new MySeries(this.series_id, this.series_title, total_seasons);
			this.image = image;
		}

		private void initiateFromDB(MyEpisode episode) {
			this.IMDB_id = episode.IMDB_id;
			this.series_id = episode.series_id;
			this.series_title = episode.series_title;
			this.season_number = episode.season_number;
			this.episode_number = episode.episode_number;
			this.episode_title = episode.episode_title;
			this.rating = episode.rating;
			this.actors = episode.actors;
			this.released = episode.released;
			this.genre = episode.genre;
			this.plot = episode.plot;
			this.image = episode.image;
			this.total_seasons = episode.total_seasons;
		}

		public void initiate() {

			this.IMDB_id = episode.ImdbId;
			this.series_id = episode.SeriesId;
			this.series = new MySeries(episode.SeriesId);
			this.series_title = this.series.title;
			this.season_number = Convert.ToInt32(episode.SeasonNumber);
			this.episode_number = Convert.ToInt32(episode.EpisodeNumber);
			this.episode_title = episode.Title;
			this.rating = episode.ImdbRating;
			DateTime date = Convert.ToDateTime(episode.Released);
			this.released = date.Year + "-" + date.Month + "-" + date.Day;
			this.actors = episode.Actors;
			this.genre = episode.Genre;
			this.plot = episode.Plot;
			this.image = UTILITY.webClient.DownloadData(episode.Poster);
			UTILITY.db.addEpisode(this);

		}

		public void destroy() {
			chk = true;
		}

		public override bool Equals(object obj) {
			return this.IMDB_id == ( (IMovie) obj).getMovieId();
		}

		public string getMovieTitle() {
			return this.series_title + " Season " + this.season_number + " Episode " + this.episode_number;
		}

		public string getMovieId() {
			return this.IMDB_id;
		}

		public MyEpisode setFileLocation(string file_location) {
			this.file_location = file_location;
			return this;
		}
	}
}
