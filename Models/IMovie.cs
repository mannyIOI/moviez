﻿namespace WpfApp2.Models {
	public interface IMovie {
		string getMovieTitle();
		string getMovieId();		
	}
}
