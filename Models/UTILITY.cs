﻿using OMDbApiNet;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace WpfApp2.Models {
	public static class UTILITY {
		public static OmdbClient omdb = new OmdbClient("f98e65ed");
		public static Database db = new Database();
		public static List<MyFeature> featureCart = new List<MyFeature>();
		public static List<MyEpisode> episodeCart = new List<MyEpisode>();
		public static WebClient webClient = new WebClient();

		public static string getCartString() {
			StringBuilder s1 = new StringBuilder();
			s1.Append($"{UTILITY.featureCart.Count} feature movies");
			s1.Append("\n");
			s1.Append("------------\n");
			for (int i = 0; i < UTILITY.featureCart.Count; i++) {
				s1.Append(UTILITY.featureCart[i].title);
				if (i != UTILITY.featureCart.Count - 1) {
					s1.Append("\n\n");
				}
			}
			s1.Append("\n\n=============================\n\n");
			s1.Append($"& {UTILITY.episodeCart.Count} series movies\n");
			s1.Append("------------");
			s1.Append("\n");
			for (int i = 0; i < UTILITY.episodeCart.Count; i++) {
				s1.Append(UTILITY.episodeCart[i].series_title +
					" Season " + UTILITY.episodeCart[i].season_number +
					" Episode " + UTILITY.episodeCart[i].episode_number
					+ " - " + UTILITY.episodeCart[i].episode_title);
				if (i != UTILITY.episodeCart.Count - 1) {
					s1.Append("\n\n");
				}
			}
			s1.Append("\n\n");
			s1.Append("------------");
			s1.Append("\n\n");
			s1.Append($"Total Price : {UTILITY.featureCart.Count * 3 + UTILITY.episodeCart.Count * 1} ETB");

			return s1.ToString();
		}

		//public static 

	}
}
