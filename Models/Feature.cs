﻿using OMDbApiNet.Model;
using System;
using System.Threading.Tasks;

namespace WpfApp2.Models {
	public class MyFeature : IMovie {
		public string IMDB_id;
		public string title;
		public string rating;
		public string released;
		public string rated;
		public string genre;
		public string director;
		public string actors;
		public string revenue;
		public string productions;
		public string plot;
		public byte[] image;
		public string file_location;
		public bool error = false;
		public string error_message;
		Item item;

		
		Database db = new Database();

		public MyFeature(string title) {
			MyFeature featFromDb = this.getFromDb(title);
			if (featFromDb != null) {
				initializeFromDB(featFromDb);
			}
			else {
				this.item = UTILITY.omdb.GetItemByTitle(title);
				initialize();
			}
		}

		public async void startAsync(string title) {
			this.item = await sth(title);
			initialize();
		}

		public async Task<Item> sth(string title) {
			//return UTILITY.omdb.GetItemByTitle(title);
			return await Task.Run(() => UTILITY.omdb.GetItemByTitle(title));
		}

		public MyFeature getFromDb(string title) {
			return UTILITY.db.getFeature(title);
		}

		public MyFeature(string IMDB_id, string title, string rating, string released, string rated,
						string genre, string director, string actors, string revenue, string productions, string plot, byte[] image) {
			this.IMDB_id = IMDB_id;
			this.title = title;
			this.rating = rating;
			this.released = released;
			this.rated = rated;
			this.genre = genre;
			this.director = director;
			this.actors = actors;
			this.revenue = revenue;
			this.productions = productions;
			this.plot = plot;
			this.image = image;
		}

		private void initialize() {
			if (item != null) {
				this.IMDB_id = item.ImdbId;
				this.title = item.Title;
				this.rating = item.ImdbRating;
				DateTime date = Convert.ToDateTime(item.Released);
				this.released = date.Year + "-" + date.Month + "-" + date.Day;
				this.rated = item.Rated;
				this.genre = item.Genre;
				this.director = item.Director;
				this.actors = item.Actors;
				this.revenue = item.BoxOffice;
				this.productions = item.Production;
				this.plot = item.Plot;
				this.image = UTILITY.webClient.DownloadData(item.Poster);
			}
		}

		public void initializeFromDB(MyFeature feature) {
			this.IMDB_id = feature.IMDB_id;
			this.title = feature.title;
			this.rating = feature.rating;
			this.released = feature.released;
			this.rated = feature.rated;
			this.genre = feature.genre;
			this.director = feature.director;
			this.actors = feature.actors;
			this.revenue = feature.revenue;
			this.productions = feature.productions;
			this.plot = feature.plot;
			this.image = feature.image;
		}

		public override bool Equals(object obj) {
			return this.IMDB_id == ((IMovie)obj).getMovieId();
		}

		public string getMovieTitle() {
			return this.title;
		}

		public string getMovieId() {
			return this.IMDB_id;
		}

		public MyFeature setFileLocation(string file_location) {
			this.file_location = file_location;
			return this;
		}
	}
}
