﻿using OMDbApiNet.Model;
using System;

namespace WpfApp2.Models {
	public class MySeries : IMovie{
		public string series_id;
		public string title;
		public int totalSeasons;
		public Item item;

		public MySeries(string series_id) {
			this.series_id = series_id;
			item = UTILITY.omdb.GetItemById(series_id);
			initialize();
		}
		public MySeries(string series_id, string title, int totalSeasons) {
			this.series_id = series_id;
			this.title = title;
			this.totalSeasons = totalSeasons;
		}

		public string getMovieId() {
			return this.series_id;
		}

		public string getMovieTitle() {
			return this.title;
		}

		private void initialize() {
			this.title = this.item.Title;
			this.totalSeasons = Convert.ToInt32(this.item.TotalSeasons);
		}

		
	}
}
