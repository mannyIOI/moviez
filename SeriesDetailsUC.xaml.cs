﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using WpfApp2.Models;
//using System.Net.WebClient;
using System.IO;

namespace WpfApp2 {
	/// <summary>
	/// Interaction logic for movie_details.xaml
	/// </summary>

	public partial class SeriesDetailsUC : UserControl {
		WebClient webClient = new WebClient();
		MyEpisode Infeature;
        bool isFromAdmin = false;
		public SeriesDetailsUC(MyEpisode episode, bool isFromAdmin) {
			InitializeComponent();
			RetrieveData(episode);
			this.Infeature = episode;
            this.isFromAdmin = isFromAdmin;

			if (UTILITY.episodeCart.Contains(episode)) {
				movie_checkBox.IsChecked = true;
			}
			
		}
		public SeriesDetailsUC(string title) {
			InitializeComponent();
			//this.Infeature
			setImageAsync(movie_poster, null);
			movie_rating.Visibility = Visibility.Hidden;
			movie_release_date.Text = "";
			movie_title.Text = "Could not find the movie "+ title;
			//rated.Text = "";
			Genre.Text = "";
			//movie_director.Text = "";
			movie_actors.Text = "";
			//revenue.Text = "";
			//production.Text = "";
			movie_plot.Text = "There may be a misspelling please contact admin ... Thank you";
		}


		private void RetrieveData(MyEpisode episode) {
			//try {

				setImageAsync(movie_poster, episode.image);
				movie_rating.Text = "EP : "+ episode.episode_number;
				movie_release_date.Text = episode.released;
				movie_title.Text = episode.series_title + " - " + episode.episode_title;
				//rated.Text = feature.rated;
				Genre.Text = episode.genre;
				season_number.Text = "Season " + episode.season_number;
				movie_actors.Text = "Actors: "+ episode.actors;
				//revenue.Text = feature.revenue;
				//production.Text = feature.pro;
				movie_plot.Text = episode.plot;

		}

		private void setImageAsync(Image image, byte[] byteImage) {
			if (byteImage != null) {
				//image.Visibility = Visibility.Visible;
				image.Source = LoadBitmapFromBytes(byteImage);
			}
			else {
				movie_poster.Source = new BitmapImage(new Uri("assets/2.gif", UriKind.Relative));
			}
		}
		public static BitmapImage LoadBitmapFromBytes(byte[] bytes) {
			var image = new BitmapImage();
			using (var stream = new MemoryStream(bytes)) {
				stream.Seek(0, SeekOrigin.Begin);
				image.BeginInit();
				image.StreamSource = stream;
				image.CacheOption = BitmapCacheOption.OnLoad;
				image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
				image.UriSource = null;
				image.EndInit();
			}
			image.Freeze();

			return image;
		}

		public BitmapImage LoadBitmapFromUri(string file) {
			var image = new BitmapImage();
			image.BeginInit();
			image.UriSource = new Uri(file, UriKind.Relative);
			image.EndInit();
			image.Freeze();
			return image;
		}

		private void checkedListener(object sender, RoutedEventArgs e) {
			
			//MessageBox.Show(Infeature.title);
			if (this.Infeature != null && !UTILITY.episodeCart.Contains(this.Infeature)) {
				UTILITY.episodeCart.Add(this.Infeature);
				//MessageBox.Show();
			}
			else if (UTILITY.episodeCart.Contains(this.Infeature)) {

			}
			else {
				//txtMovieMessage.Text = "Sorry This movie is not available";
				//movie_message.IsOpen = true;
			}
			
		}

		private void movieUncheckedListener(object sender, RoutedEventArgs e) {
			if (this.Infeature != null) {
				UTILITY.episodeCart.Remove(this.Infeature);
			}
		}

		private void mouseHovered(object sender, MouseEventArgs e) {

			if (this.Infeature != null && UTILITY.episodeCart.Contains(this.Infeature)) {
				addCartIcon.Visibility = Visibility.Hidden;
				removeCartIcon.Visibility = Visibility.Visible;
				addOrRemoveCartText.Text = "Remove From Cart";
			}
			else {
				addCartIcon.Visibility = Visibility.Visible;
				removeCartIcon.Visibility = Visibility.Hidden;
				addOrRemoveCartText.Text = "Add To Cart";
			}
            if (!isFromAdmin) {
                movie_message.IsOpen = true;
            }
			
		}

		private void Grid_MouseLeave(object sender, MouseEventArgs e) {
			movie_message.IsOpen = false;
		}

		private void AddOrRemoveFromCart(object sender, RoutedEventArgs e) {
			if (this.Infeature != null && !UTILITY.episodeCart.Contains(this.Infeature)) {
				UTILITY.episodeCart.Add(this.Infeature);
				movie_checkBox.IsChecked = true;
			}
			else {
				UTILITY.episodeCart.Remove(this.Infeature);
				movie_checkBox.IsChecked = false;
			}
		}
	}
}

