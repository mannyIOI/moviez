﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using WpfApp2.Models;

namespace WpfApp2 {

	public partial class GuestPage : Page {
		MaterialDesignThemes.Wpf.PackIcon active;
		//Page current;
		public GuestPage() {
			InitializeComponent();
			active = trendingActive;
			loadTrending();
			active.Visibility = Visibility.Visible;
		}

		private void loadTrending() {
			//current.Dispose();
			contentPage.Navigate(new Trending(loading));

		}
		private void loadSeries() {
			try {
				contentPage.Navigate(new Series());
			}
			catch (Exception ex) {
				MessageBox.Show(ex.ToString());
			}
		}
		private void loadMovies() {
			contentPage.Navigate(new Movies());
		}


		private void OnSeriesSelected(object sender, MouseButtonEventArgs e) {
			active.Visibility = Visibility.Hidden;
			active = seriesActive;
			active.Visibility = Visibility.Visible;
			loadSeries();
		}

		private void OnMoviesSelected(object sender, MouseButtonEventArgs e) {
			active.Visibility = Visibility.Hidden;
			active = moviesActive;
			active.Visibility = Visibility.Visible;
			loadMovies();
		}

		private void OnTrendingSelected(object sender, MouseButtonEventArgs e) {
			active.Visibility = Visibility.Hidden;
			active = trendingActive;
			active.Visibility = Visibility.Visible;
			loadTrending();
		}

		private void Logout(object sender, RoutedEventArgs e) {
			UTILITY.featureCart.Clear();
			UTILITY.episodeCart.Clear();
			NavigationService.Navigate(new Login());
		}
	}
}
