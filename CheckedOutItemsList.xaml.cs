﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using WpfApp2.Models;

namespace WpfApp2 {
	/// <summary>
	/// Interaction logic for CheckedOutItemsList.xaml
	/// </summary>
	public partial class CheckedOutItemsList : Page {
		string user_id;
		List<CheckedOut> elements;
		public CheckedOutItemsList(List<CheckedOut> elements, string user_id) {
			InitializeComponent();
			this.user_id = user_id;
			this.elements = elements;
			for (int i = 0; i < elements.Count;i++) {
				ListBoxItem item = new ListBoxItem();
				item.Content = elements[i].title;
				movie_list.Items.Add(item);
			}
		}

		private void removeCheckOut(object sender, RoutedEventArgs e) {
			List<string> fromList = new List<string>();
			List<string> toList = new List<string>();
			string folderLocation = "";

			FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
			
			if (folderBrowser.ShowDialog() == DialogResult.OK) {
				folderLocation = folderBrowser.SelectedPath;
			}

			foreach (CheckedOut element in elements) {
				fromList.Add(element.file_location);
				string[] fileLocArray = element.file_location.Split('\\');
			
				string to = $@"{folderLocation}{fileLocArray[fileLocArray.Length - 1]}";
				toList.Add(to);
			}
			copyMultiple(fromList, toList);

		}

		public async void copyMultiple(List<string> fromList, List<string> toList) {

			var list = new List<List<string>>();

			for (int i = 0, j = 0; i < fromList.Count && j < toList.Count; i++, j++) {
				var srcDest = new List<string>();
				srcDest.Add(fromList[i]);
				srcDest.Add(toList[j]);
				list.Add(srcDest);
			}

			//System.Windows.MessageBox.Show(""+Copier.CopyFiles(list));
			//System.Windows.MessageBox.Show("E:\\Videos\\Feature Movies\\source\\1.mp4");
			//var srcDest = new List<string>();
			//srcDest.Add();
			//srcDest.Add("E:\\Videos\\Feature Movies\\dest\\1.mp4");
			//list.Add(srcDest);

			//var list = new Dictionary<string, string> {
			//	{ "E:\\Videos\\Feature Movies\\source\\1.mp4", "D:\\1.mp4" }
			//};
			prgBaseMap.Maximum = 10000.0;
			await Copier.CopyFiles(list, prog => prgBaseMap.Value = prog);
			System.Windows.MessageBox.Show("User ID - "+user_id+" has finished!!");
		}

	}
}
