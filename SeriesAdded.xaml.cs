﻿using System.Windows.Controls;
using WpfApp2;
using WpfApp2.Models;

namespace MovieZ {
	/// <summary>
	/// Interaction logic for Page1.xaml
	/// </summary>
	public partial class SeriesAdded : Page {
		public SeriesAdded(MyEpisode ep) {
			InitializeComponent();
			seriesContent.Children.Add(new SeriesDetailsUC(ep, true));
		}
	}
}
